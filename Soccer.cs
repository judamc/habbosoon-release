namespace Plus.HabboHotel.Rooms.Games.Football
{
    using Plus.Communication.Packets.Outgoing.Rooms.Engine;
    using Plus.HabboHotel.Items;
    using Plus.HabboHotel.Items.Wired;
    using Plus.HabboHotel.Pathfinding;
    using Plus.HabboHotel.Rooms;
    using Plus.HabboHotel.Rooms.Games.Teams;
    using System;
    using System.Collections.Concurrent;
    using System.Drawing;
    using System.Linq;
    using System.Runtime.InteropServices;

    public class Soccer
    {
        private ConcurrentDictionary<int, Item> _balls;
        private bool _gameStarted;
        private Room _room;
        private Item[] gates;

        public Soccer(Room room)
        {
            this._room = room;
            this.gates = new Item[4];
            this._balls = new ConcurrentDictionary<int, Item>();
            this._gameStarted = false;
        }

        public void AddBall(Item item)
        {
            this._balls.TryAdd(item.Id, item);
        }

        public void Dispose()
        {
            Array.Clear(this.gates, 0, this.gates.Length);
            this.gates = null;
            this._room = null;
            this._balls.Clear();
            this._balls = null;
        }

        public void MoveBall(Item item, int newX, int newY, RoomUser user)
        {
            if (((item != null) && (user != null)) && this._room.GetGameMap().itemCanBePlacedHere(newX, newY))
            {
                Point coordinate = item.Coordinate;
                if ((coordinate.X != newX) || (coordinate.Y != newY))
                {
                    double toZ = this._room.GetGameMap().Model.SqFloorHeight[newX, newY];
                    this._room.SendMessage(new SlideObjectBundleComposer(item.Coordinate.X, item.Coordinate.Y, item.GetZ, newX, newY, toZ, item.Id, item.Id, item.Id), false);
                    item.ExtraData = "11";
                    item.UpdateNeeded = true;
                    this._room.GetRoomItemHandler().SetFloorItem(null, item, newX, newY, item.Rotation, false, false, false, false);
                    this._room.OnUserShoot(user, item);
                }
            }
        }

        public void onGateRemove(Item item)
        {
            switch (item.GetBaseItem().InteractionType)
            {
                case InteractionType.FOOTBALL_GOAL_GREEN:
                case InteractionType.footballcountergreen:
                    this._room.GetGameManager().RemoveFurnitureFromTeam(item, TEAM.GREEN);
                    break;

                case InteractionType.FOOTBALL_GOAL_YELLOW:
                case InteractionType.footballcounteryellow:
                    this._room.GetGameManager().RemoveFurnitureFromTeam(item, TEAM.YELLOW);
                    break;

                case InteractionType.FOOTBALL_GOAL_BLUE:
                case InteractionType.footballcounterblue:
                    this._room.GetGameManager().RemoveFurnitureFromTeam(item, TEAM.BLUE);
                    break;

                case InteractionType.FOOTBALL_GOAL_RED:
                case InteractionType.footballcounterred:
                    this._room.GetGameManager().RemoveFurnitureFromTeam(item, TEAM.RED);
                    break;
            }
        }

        public void OnUserWalk(RoomUser User)
        {
            if (User != null)
            {
                foreach (Item item in this._balls.Values.ToList<Item>())
                {
                    int newX = 0;
                    int newY = 0;
                    int num3 = User.X - item.GetX;
                    int num4 = User.Y - item.GetY;
                    if ((num3 == 0) && (num4 == 0))
                    {
                        if (User.RotBody == 4)
                        {
                            newX = User.X;
                            newY = User.Y + 2;
                        }
                        else if (User.RotBody == 6)
                        {
                            newX = User.X - 2;
                            newY = User.Y;
                        }
                        else if (User.RotBody == 0)
                        {
                            newX = User.X;
                            newY = User.Y - 2;
                        }
                        else if (User.RotBody == 2)
                        {
                            newX = User.X + 2;
                            newY = User.Y;
                        }
                        else if (User.RotBody == 1)
                        {
                            newX = User.X + 2;
                            newY = User.Y - 2;
                        }
                        else if (User.RotBody == 7)
                        {
                            newX = User.X - 2;
                            newY = User.Y - 2;
                        }
                        else if (User.RotBody == 3)
                        {
                            newX = User.X + 2;
                            newY = User.Y + 2;
                        }
                        else if (User.RotBody == 5)
                        {
                            newX = User.X - 2;
                            newY = User.Y + 2;
                        }
                        if (!this._room.GetRoomItemHandler().CheckPosItem(User.GetClient(), item, newX, newY, item.Rotation, false, false))
                        {
                            if (User.RotBody == 0)
                            {
                                newX = User.X;
                                newY = User.Y + 1;
                            }
                            else if (User.RotBody == 2)
                            {
                                newX = User.X - 1;
                                newY = User.Y;
                            }
                            else if (User.RotBody == 4)
                            {
                                newX = User.X;
                                newY = User.Y - 1;
                            }
                            else if (User.RotBody == 6)
                            {
                                newX = User.X + 1;
                                newY = User.Y;
                            }
                            else if (User.RotBody == 5)
                            {
                                newX = User.X + 1;
                                newY = User.Y - 1;
                            }
                            else if (User.RotBody == 3)
                            {
                                newX = User.X - 1;
                                newY = User.Y - 1;
                            }
                            else if (User.RotBody == 7)
                            {
                                newX = User.X + 1;
                                newY = User.Y + 1;
                            }
                            else if (User.RotBody == 1)
                            {
                                newX = User.X - 1;
                                newY = User.Y + 1;
                            }
                        }
                    }
                    else if ((((num3 <= 1) && (num3 >= -1)) && ((num4 <= 1) && (num4 >= -1))) && this.VerifyBall(User, item.Coordinate.X, item.Coordinate.Y))
                    {
                        newX = num3 * -1;
                        newY = num4 * -1;
                        newX += item.GetX;
                        newY += item.GetY;
                    }
                    if (item.GetRoom().GetGameMap().ValidTile(newX, newY))
                    {
                        this.MoveBall(item, newX, newY, User);
                    }
                }
            }
        }

        public void RegisterGate(Item item)
        {
            if (this.gates[0] == null)
            {
                item.team = TEAM.BLUE;
                this.gates[0] = item;
            }
            else if (this.gates[1] == null)
            {
                item.team = TEAM.RED;
                this.gates[1] = item;
            }
            else if (this.gates[2] == null)
            {
                item.team = TEAM.GREEN;
                this.gates[2] = item;
            }
            else if (this.gates[3] == null)
            {
                item.team = TEAM.YELLOW;
                this.gates[3] = item;
            }
        }

        public void RemoveBall(int itemID)
        {
            Item item = null;
            this._balls.TryRemove(itemID, out item);
        }

        public void StartGame()
        {
            this._gameStarted = true;
        }

        public void StopGame(bool userTriggered = false)
        {
            this._gameStarted = false;
            if (!userTriggered)
            {
                this._room.GetWired().TriggerEvent(WiredBoxType.TriggerGameEnds, null);
            }
        }

        public void UnRegisterGate(Item item)
        {
            switch (item.team)
            {
                case TEAM.RED:
                    this.gates[1] = null;
                    break;

                case TEAM.GREEN:
                    this.gates[2] = null;
                    break;

                case TEAM.BLUE:
                    this.gates[0] = null;
                    break;

                case TEAM.YELLOW:
                    this.gates[3] = null;
                    break;
            }
        }

        private bool VerifyBall(RoomUser user, int actualx, int actualy) =>
            (Rotation.Calculate(user.X, user.Y, actualx, actualy) == user.RotBody);

        public bool GameIsStarted =>
            this._gameStarted;
    }
}

