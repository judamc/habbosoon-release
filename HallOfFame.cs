ttrusing Plus.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.LandingView
{
    public class HallOfFame
    {

        public List<hallOfFameWinner> BestList;
        public HallOfFame()
        {
            BestList = new List<hallOfFameWinner>();
            BestList.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `hall_of_fame`");
                DataTable best = dbClient.getTable();

                if (best != null)
                {
                    foreach (DataRow Data in best.Rows)
                    {
                        BestList.Add(new hallOfFameWinner(Data["username"].ToString(), Data["look"].ToString(), Convert.ToInt32(Data["place"]), Convert.ToInt32(Data["id"])));
                    }
                }
            }
            return;
        }


        public void Init()
        {
            BestList = new List<hallOfFameWinner>();
            BestList.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `hall_of_fame`");
                DataTable best = dbClient.getTable();

                if (best != null)
                {
                    foreach (DataRow Data in best.Rows)
                    {
                        BestList.Add(new hallOfFameWinner(Data["username"].ToString(), Data["look"].ToString(), Convert.ToInt32(Data["place"]), Convert.ToInt32(Data["id"])));
                    }
                }
            }
        }

        public class hallOfFameWinner
        {
            public string username;
            public int place;
            public string look;
            public int id;

            public hallOfFameWinner(string username, string look, int place, int id)
            {
                this.username = username;
                this.place = place;
                this.look = look;
                this.id = id;
            }
        }
    }
}